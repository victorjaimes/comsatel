package com.example.comsatel.Fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.comsatel.Adapter.pendientesAdapter;
import com.example.comsatel.Model.pendientes;
import com.example.comsatel.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PendientesFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private String mParam1;
    private String mParam2;


    private RequestQueue requestQueue;
    private SwipeRefreshLayout refresh;
    private ArrayList<pendientes> pendientes = new ArrayList<>();
    private JsonArrayRequest arrayRequest;
    private RecyclerView recyclerView;
    private Dialog dialog;
    private pendientesAdapter pendientesAdapter;

    private String url="http://192.168.2.104/comsatel/api/pendientes/pendientes.php";
    private String url_add="http://192.168.2.104/comsatel/api/pendientes/add_list.php";

    private TextView tv_close, tv_title;
    private EditText et_add;
    private Button btn_add;
    private FloatingActionButton fab_add;

    public PendientesFragment() {
        // Required empty public constructor
    }

    public static PendientesFragment newInstance(String param1, String param2) {
        PendientesFragment fragment = new PendientesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pendientes, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        refresh = (SwipeRefreshLayout) view.findViewById(R.id.sfl_p);
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_pendientes);
        fab_add = (FloatingActionButton) view.findViewById(R.id.fab_app);

        dialog = new Dialog(getContext());

        refresh.setOnRefreshListener(this);
        refresh.post(new Runnable() {
            @Override
            public void run() {
                getData();
                pendientes.clear();
            }
        });

        fab_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addPendientes(view);
            }
        });

    }

    private void getData() {
        refresh.setRefreshing(true);

        arrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                JSONObject jsonObject = null;
                for (int i = 0; i < response.length(); i++) {
                    try {
                        jsonObject = response.getJSONObject(i);

                        pendientes pe = new pendientes();
                        pe.setDescripcion(jsonObject.getString("descripcion"));
                        pe.setId(jsonObject.getInt("id"));
                        pe.setCant(jsonObject.getInt("cant"));
                        pendientes.add(pe);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                adapterpush(pendientes);
                refresh.setRefreshing(false);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(arrayRequest);
    }

    private void adapterpush(ArrayList<pendientes> pendientes) {
        pendientesAdapter = new pendientesAdapter(getContext(),pendientes);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(pendientesAdapter);
    }

    public void addPendientes(View view){
        dialog.setContentView(R.layout.add_edit_pendientes);

        tv_close = (TextView) dialog.findViewById(R.id.tv_close);
        tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        tv_title.setText("Agregar item");
        tv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        et_add = (EditText) dialog.findViewById(R.id.et_add_p);
        btn_add = (Button) dialog.findViewById(R.id.btn_add_p);
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String data = et_add.getText().toString();
                if(data.isEmpty()){
                }else{
                    btnadd(data);
                }

            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void btnadd(final String data) {
        StringRequest request =  new StringRequest(Request.Method.POST, url_add, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();
                refresh.post(new Runnable() {
                    @Override
                    public void run() {
                        pendientes.clear();
                        getData();
                    }
                });

                Toast.makeText(getActivity(), "Se realizó el cambio.", Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "No realizó el cambio.", Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params =  new HashMap<>();
                params.put("descripcion",data);


                return params;
            }
        };

        Volley.newRequestQueue(getContext()).add(request);

    }

    @Override
    public void onRefresh(){
        pendientes.clear();
        getData();
    }

}