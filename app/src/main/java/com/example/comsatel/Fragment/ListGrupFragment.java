package com.example.comsatel.Fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.comsatel.Adapter.tareas_pendientesAdapter;
import com.example.comsatel.Model.tareas_pendientes;
import com.example.comsatel.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ListGrupFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListGrupFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private Toolbar toolbar;
    private String id_p, desc_p;
    private TextView title_toolbar;

    private RequestQueue requestQueue;
    private SwipeRefreshLayout refresh;
    private ArrayList<tareas_pendientes> tareas_pendientes= new ArrayList<>();
    private JsonArrayRequest arrayRequest;
    private RecyclerView recyclerView;
    private Dialog dialog;
    private tareas_pendientesAdapter tareas_pendientesAdapter;

    public String url,url_add;

    private TextView tv_close, tv_title;
    private EditText et_add;
    private Button btn_add;
    private FloatingActionButton fab_add;

    public ListGrupFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ListGrupFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ListGrupFragment newInstance(String param1, String param2) {
        ListGrupFragment fragment = new ListGrupFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_list_grup, container, false);


    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        id_p = getArguments().getString("ID_P");
        desc_p = getArguments().getString("DES_P");
        url="http://192.168.2.104/comsatel/api/pendientes/tareas_pendientes.php?id="+id_p;
        url_add="http://192.168.2.104/comsatel/api/pendientes/add_list_tareas.php?id="+id_p;

        title_toolbar = (TextView)view.findViewById(R.id.tv_tb_tareas);
        title_toolbar.setText(""+desc_p);

         toolbar = (Toolbar) view.findViewById(R.id.toolbar_t);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });


        refresh = (SwipeRefreshLayout) view.findViewById(R.id.sfl_t);
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_tareas);
        fab_add = (FloatingActionButton) view.findViewById(R.id.fab_tareas);

        dialog = new Dialog(getContext());

        refresh.setOnRefreshListener(this);
        refresh.post(new Runnable() {
            @Override
            public void run() {
                getData();
                tareas_pendientes.clear();
            }
        });

        fab_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addTareasPendientes(view);
            }
        });
    }

    private void getData() {
        refresh.setRefreshing(true);

        arrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                JSONObject jsonObject = null;
                for (int i = 0; i < response.length(); i++) {
                    try {
                        jsonObject = response.getJSONObject(i);

                        tareas_pendientes pe = new tareas_pendientes();
                        pe.setDescripcion(jsonObject.getString("descripcion"));
                        pe.setId(jsonObject.getInt("id"));
                        tareas_pendientes.add(pe);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                adapterpush(tareas_pendientes);
                refresh.setRefreshing(false);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(arrayRequest);
    }

    private void adapterpush(ArrayList<tareas_pendientes> tareas_pendientes) {
        tareas_pendientesAdapter = new tareas_pendientesAdapter(getContext(),tareas_pendientes);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(tareas_pendientesAdapter);
    }

    public void addTareasPendientes(View view){
        dialog.setContentView(R.layout.add_edit_tareas_pendientes);

        tv_close = (TextView) dialog.findViewById(R.id.tv_close_t);
        tv_title = (TextView) dialog.findViewById(R.id.tv_title_t);
        tv_title.setText("Agregar item");
        tv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        et_add = (EditText) dialog.findViewById(R.id.et_add_t);
        btn_add = (Button) dialog.findViewById(R.id.btn_add_t);
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String data = et_add.getText().toString();
                if(data.isEmpty()){
                }else{
                    btnadd(data);
                }

            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void btnadd(final String data) {
        StringRequest request =  new StringRequest(Request.Method.POST, url_add, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();
                refresh.post(new Runnable() {
                    @Override
                    public void run() {
                        tareas_pendientes.clear();
                        getData();
                    }
                });

                Toast.makeText(getActivity(), "Se realizó el cambio.", Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "No realizó el cambio.", Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params =  new HashMap<>();
                params.put("descripcion",data);


                return params;
            }
        };

        Volley.newRequestQueue(getContext()).add(request);
    }

    @Override
    public void onRefresh() {
        tareas_pendientes.clear();
        getData();
    }
}