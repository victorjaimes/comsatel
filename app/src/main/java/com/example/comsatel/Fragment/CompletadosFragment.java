package com.example.comsatel.Fragment;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.comsatel.Adapter.completadosAdapter;
import com.example.comsatel.Model.completados;
import com.example.comsatel.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CompletadosFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CompletadosFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private RequestQueue requestQueue;
    private SwipeRefreshLayout refresh;
    private ArrayList<completados> completados = new ArrayList<>();
    private JsonArrayRequest arrayRequest;
    private RecyclerView recyclerView;
    private Dialog dialog;
    private completadosAdapter completadosAdapter;

    private String url="http://192.168.2.104/comsatel/api/completados/completados.php";

    public CompletadosFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static CompletadosFragment newInstance(String param1, String param2) {
        CompletadosFragment fragment = new CompletadosFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_completados, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        refresh = (SwipeRefreshLayout) view.findViewById(R.id.sfl_c);
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_completados);

        dialog = new Dialog(getContext());

        refresh.setOnRefreshListener(this);
        refresh.post(new Runnable() {
            @Override
            public void run() {
                getData();
                completados.clear();
            }
        });
    }

    private void getData() {
        refresh.setRefreshing(true);

        arrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                JSONObject jsonObject = null;
                for (int i = 0; i < response.length(); i++) {
                    try {
                        jsonObject = response.getJSONObject(i);

                        completados co = new completados();
                        co.setDescripcion(jsonObject.getString("descripcion"));
                        co.setId(jsonObject.getInt("id"));
                        co.setCant(jsonObject.getInt("cant"));
                        completados.add(co);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                adapterpush(completados);
                refresh.setRefreshing(false);
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(arrayRequest);
    }

    private void adapterpush(ArrayList<completados> completados) {
        completadosAdapter = new completadosAdapter(getContext(),completados);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(completadosAdapter);
    }


    @Override
    public void onRefresh() {
        completados.clear();
        getData();
    }
}