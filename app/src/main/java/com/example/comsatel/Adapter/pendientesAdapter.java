package com.example.comsatel.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.comsatel.Model.pendientes;
import com.example.comsatel.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class pendientesAdapter extends RecyclerView.Adapter<pendientesAdapter.ViewHolder> {
    private Context context;
    private ArrayList<pendientes> pendientes;
    private String url_edit="http://192.168.2.104/comsatel/api/pendientes/edit_list.php";
    private String url_delete="http://192.168.2.104/comsatel/api/pendientes/delete_list.php";

    public pendientesAdapter(Context context, ArrayList<pendientes> pendientes){
        this.context= context;
        this.pendientes = pendientes;
    }


    @NonNull
    @Override
    public pendientesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        view = layoutInflater.inflate(R.layout.pendientes_list, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull pendientesAdapter.ViewHolder holder, final int position) {
        holder.desc.setText(pendientes.get(position).getDescripcion()+" - ("+pendientes.get(position).getCant()+" items)");
        holder.edit_p.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int id= pendientes.get(position).getId();
                String value = pendientes.get(position).getDescripcion();

                editPedientes(id,value);
            }
        });

        holder.delete_p.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int id= pendientes.get(position).getId();
                deletePedientes(id);
            }
        });
    }

    private void deletePedientes(final int id) {

        TextView tv_close, tv_title;
        Button btn_delete;
        final Dialog dialog;

        dialog = new Dialog(context);
        dialog.setContentView(R.layout.delete_pendientes);

        tv_close = (TextView) dialog.findViewById(R.id.tv_close);
        tv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        btn_delete = (Button) dialog.findViewById(R.id.btn_delete_p);
        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnadd("DELETE","",dialog, id);

            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

    }

    private void editPedientes(final int id, String value) {
        TextView tv_close, tv_title;
        final EditText et_add;
        Button btn_add;
        FloatingActionButton fab_add;
        final Dialog dialog;

        dialog = new Dialog(context);
        dialog.setContentView(R.layout.add_edit_pendientes);

        tv_close = (TextView) dialog.findViewById(R.id.tv_close);
        tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        tv_title.setText("Editar");
        tv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        et_add = (EditText) dialog.findViewById(R.id.et_add_p);
        et_add.setHint("Editar item");
        et_add.setText(value);
        btn_add = (Button) dialog.findViewById(R.id.btn_add_p);
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String data = et_add.getText().toString();
                if(data.isEmpty()){
                }else{
                    btnadd("PUT",data,dialog, id);
                }

            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void btnadd(String method, final String data, final Dialog dialog, final int id) {

        if(method == "PUT") {

            StringRequest request = new StringRequest(Request.Method.POST, url_edit, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    dialog.dismiss();

                    Toast.makeText(context, "Se realizó el cambio.", Toast.LENGTH_LONG).show();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(context, "No realizó el cambio.", Toast.LENGTH_LONG).show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("descripcion", data);
                    params.put("id", String.valueOf(id));

                    return params;
                }
            };

            Volley.newRequestQueue(context).add(request);

        }else if(method == "DELETE"){

            StringRequest request = new StringRequest(Request.Method.POST, url_delete, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    dialog.dismiss();

                    Toast.makeText(context, "Se realizó el cambio.", Toast.LENGTH_LONG).show();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(context, "No realizó el cambio.", Toast.LENGTH_LONG).show();
                }
            }){
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("id", String.valueOf(id));

                    return params;
                }
            };

            Volley.newRequestQueue(context).add(request);

        }

    }

    @Override
    public int getItemCount() {
        return pendientes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView desc, no;
        private ImageView edit_p, delete_p;
        private String id_list;
        private CardView cv_p;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            desc = (TextView) itemView.findViewById(R.id.tv_pendientes);
            edit_p = (ImageView) itemView.findViewById(R.id.iv_edit_p);
            delete_p = (ImageView) itemView.findViewById(R.id.iv_delete_p);
            cv_p = (CardView) itemView.findViewById(R.id.cv_p);
            cv_p.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position  =   getAdapterPosition();
            int id = pendientes.get(position).getId();
            String value = pendientes.get(position).getDescripcion();

            switch (view.getId()){
                case R.id.cv_p:
                    //Log.w("", "Selected"+position+1);
                    Bundle bundle = new Bundle();
                    bundle.putString("ID_P", ""+id);
                    bundle.putString("DES_P", ""+value);

                    Navigation.findNavController(view).navigate(R.id.listGrupFragment, bundle);
                    break;
            }
        }
    }
}
