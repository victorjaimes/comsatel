package com.example.comsatel.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.comsatel.Model.tareas_pendientes;
import com.example.comsatel.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class tareas_pendientesAdapter extends RecyclerView.Adapter<tareas_pendientesAdapter.ViewHolder> {

    private Context context;
    private ArrayList<tareas_pendientes> tareas_pendientes;
    private String url_edit="http://192.168.2.104/comsatel/api/pendientes/edit_list_tareas.php";
    private String url_delete="http://192.168.2.104/comsatel/api/pendientes/delete_list_tareas.php";
    public String url_check;

    public tareas_pendientesAdapter(Context context, ArrayList<com.example.comsatel.Model.tareas_pendientes> tareas_pendientes) {
        this.context = context;
        this.tareas_pendientes = tareas_pendientes;
    }


    @NonNull
    @Override
    public tareas_pendientesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        view = layoutInflater.inflate(R.layout.pendientes_tareas_list, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull tareas_pendientesAdapter.ViewHolder holder, final int position) {
        holder.desc.setText(tareas_pendientes.get(position).getDescripcion());

        holder.cb_t.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                int id= tareas_pendientes.get(position).getId();
                if (compoundButton.isShown()) {
                    if (compoundButton.isChecked()) {
                        url_check="http://192.168.2.104/comsatel/api/pendientes/check_list.php?act=Si";
                    } else {
                        url_check="http://192.168.2.104/comsatel/api/pendientes/check_list.php?act=No";
                    }
                    checkTareasPendientes(id);
                }
            }
        });

        holder.edit_t.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int id= tareas_pendientes.get(position).getId();
                String value = tareas_pendientes.get(position).getDescripcion();

                editPedientes(id,value);
            }
        });

        holder.delete_t.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int id= tareas_pendientes.get(position).getId();
                deletePedientes(id);
            }
        });
    }

    private void deletePedientes(final int id) {
        TextView tv_close, tv_title;
        Button btn_delete;
        final Dialog dialog;

        dialog = new Dialog(context);
        dialog.setContentView(R.layout.delete_tareas_pendientes);

        tv_close = (TextView) dialog.findViewById(R.id.tv_close_t);
        tv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        btn_delete = (Button) dialog.findViewById(R.id.btn_delete_t);
        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnadd("DELETE","",dialog, id);

            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

    }


    private void editPedientes(final int id, String value) {
        TextView tv_close, tv_title;
        final EditText et_add;
        Button btn_add;
        final Dialog dialog;

        dialog = new Dialog(context);
        dialog.setContentView(R.layout.add_edit_tareas_pendientes);

        tv_close = (TextView) dialog.findViewById(R.id.tv_close_t);
        tv_title = (TextView) dialog.findViewById(R.id.tv_title_t);
        tv_title.setText("Editar item");
        tv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        et_add = (EditText) dialog.findViewById(R.id.et_add_t);
        et_add.setHint("Editar item");
        et_add.setText(value);
        btn_add = (Button) dialog.findViewById(R.id.btn_add_t);
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String data = et_add.getText().toString();
                if(data.isEmpty()){
                }else{
                    btnadd("PUT",data,dialog, id);
                }

            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void btnadd(String method, final String data, final Dialog dialog, final int id) {
        if(method == "PUT") {

            StringRequest request = new StringRequest(Request.Method.POST, url_edit, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    dialog.dismiss();

                    Toast.makeText(context, "Se realizó el cambio.", Toast.LENGTH_LONG).show();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(context, "No realizó el cambio.", Toast.LENGTH_LONG).show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("descripcion", data);
                    params.put("id", String.valueOf(id));

                    return params;
                }
            };

            Volley.newRequestQueue(context).add(request);

        }else if(method == "DELETE"){

            StringRequest request = new StringRequest(Request.Method.POST, url_delete, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    dialog.dismiss();

                    Toast.makeText(context, "Se realizó el cambio.", Toast.LENGTH_LONG).show();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(context, "No realizó el cambio.", Toast.LENGTH_LONG).show();
                }
            }){
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("id", String.valueOf(id));

                    return params;
                }
            };

            Volley.newRequestQueue(context).add(request);

        }
    }


    private void checkTareasPendientes(final int id) {
        StringRequest request = new StringRequest(Request.Method.POST, url_check, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id", String.valueOf(id));

                return params;
            }
        };

        Volley.newRequestQueue(context).add(request);
    }
    
    
    

    @Override
    public int getItemCount() {
        return tareas_pendientes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView desc, no;
        private ImageView edit_t, delete_t;
        private String id_list;
        private CardView cv_t;
        private CheckBox cb_t;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            desc = (TextView) itemView.findViewById(R.id.tv_tareas);
            edit_t = (ImageView) itemView.findViewById(R.id.iv_edit_t);
            delete_t = (ImageView) itemView.findViewById(R.id.iv_delete_t);
            cv_t = (CardView) itemView.findViewById(R.id.cv_t);
            cb_t =(CheckBox) itemView.findViewById(R.id.cb_tareas);

        }
    }
}
