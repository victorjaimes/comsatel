package com.example.comsatel.Model;

public class tareas_pendientes {
    int id;
    int id_p;
    String descripcion;
    String activo;
    boolean isChecked;

    public tareas_pendientes() {
    }

    public tareas_pendientes(int id, int id_p, String descripcion, String activo, boolean isChecked) {
        this.id = id;
        this.id_p = id_p;
        this.descripcion = descripcion;
        this.activo = activo;
        this.isChecked = isChecked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public int getId_p() {
        return id_p;
    }

    public void setId_p(int id_p) {
        this.id_p = id_p;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }
}
