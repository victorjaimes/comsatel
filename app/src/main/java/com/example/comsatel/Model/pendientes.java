package com.example.comsatel.Model;

public class pendientes {
    int id;
    String descripcion;
    int cant;

    public pendientes() {
    }

    public pendientes(int id, String descripcion, int cant) {
        this.id = id;
        this.descripcion = descripcion;
        this.cant= cant;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCant() {
        return cant;
    }

    public void setCant(int cant) {
        this.cant = cant;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
